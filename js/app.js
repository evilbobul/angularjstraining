var app = angular.module('calculator', []);

app.constant('digits', ['7','8','9','4','5','6','1','2','3','0', '.']); //constant just to try this
app.value('operators', ['C','+', '-', '/', '*']);
app.factory('equate', function(){
    return {
        '+': function (x, y) { return (+x) + (+y) },
        '-': function (x, y) { return (+x) - (+y) },
        '*': function (x, y) { return (+x) * (+y) },
        '/': function (x, y) { return (+x) / (+y) }
    };
});

app.directive("calculatorDirective", function () {
    return {
        restrict: "EA",
        replace: true,
        templateUrl: "directives/calculator.html"
    }
})


app.controller('CalcController', function($scope, digits, operators, equate) {

    $scope.digits = digits;
    $scope.operators = operators;
    $scope.operatorPassed = false;
    $scope.first = "";
    $scope.second = "";
    $scope.operator = "";
    $scope.typeSymbol = function(number){
        if(!$scope.operatorPassed){
            $scope.first += number;
        }else{
            $scope.second += number;
        }
    };
    $scope.clean = function () {
        $scope.first = "";
        $scope.second = "";
        $scope.operator = "";
        $scope.operatorPassed = false;
    }
    $scope.addOperator = function(operator){
        if (operator == "C"){
            $scope.clean();
            return;
        }
        $scope.operatorPassed = true;
        $scope.operator = operator;
    };
    $scope.count = function(){
        $scope.first = equate[$scope.operator]($scope.first,$scope.second);
        $scope.second = "";
        $scope.operator = "";
    }
});
